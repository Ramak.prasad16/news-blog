import styles from "../../styles/EOM.module.css";
import { Toolbar } from "@/components/toolbar";

export const EOM = ({ employee }) => {
  return (
    <>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-69V48KNY59"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments)};
gtag('js', new Date());

gtag('config', 'G-69V48KNY59');
</script>
<div className="page-container">
<Toolbar />
<div className={styles.main}>
<h1>Employee Of The Month</h1>

<div className={styles.employeeOfTheMonth}>
<h3>{employee.name}</h3>
<h6>{employee.position}</h6>
<img src={employee.image} />
<p>{employee.description}</p>
</div>
</div>
</div></>
  );
};
export const getServerSideProps = async (pageContext) => {
  const apiResponse = await fetch(
    "https://my-json-server.typicode.com/RPRNAIR/demo/employeeOfTheMonth"
  );
  const employee = await apiResponse.json();

  return {
    props: {
      employee,
    },
  };
};

export default EOM;
