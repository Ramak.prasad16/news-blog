import { Toolbar } from "@/components/toolbar";
import Head from "next/head";
// import { Inter } from "next/font/google";
import styles from "/styles/Home.module.css";

// const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-69V48KNY59"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments)};
gtag('js', new Date());

gtag('config', 'G-69V48KNY59');
</script>

    <div className="page-container">
      <Toolbar />
      <div className={styles.main}>
        <h1>Next.js News App</h1>

        <h3>Your One stop shop for the latest news articles</h3>
      </div>
    </div>
    </>
  );
}
